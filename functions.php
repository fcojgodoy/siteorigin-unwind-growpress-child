<?php

/**
 * Enqueue theme scripts and styles.
 */
function siteorigin_unwind_child_scripts() {

	// Child theme stylesheet.
	wp_enqueue_style( 'unwind-child', get_stylesheet_directory_uri() . '/assets/main.css' );

}
add_action( 'wp_enqueue_scripts', 'siteorigin_unwind_child_scripts', 1 );

// General function require files
// require get_stylesheet_directory() . '/inc/change-text-strings.php';
require get_stylesheet_directory() . '/inc/hide-admin-notifications.php';

// Woocommerce function require files
require get_stylesheet_directory() . '/inc/woocommerce/archive-product.php';
require get_stylesheet_directory() . '/inc/woocommerce/single-product-actions.php';
require get_stylesheet_directory() . '/inc/woocommerce/single-product-filters.php';
require get_stylesheet_directory() . '/inc/woocommerce/single-product-tabs.php';
require get_stylesheet_directory() . '/inc/woocommerce/wpb-woocommerce-products-slider.php';

// Register sidebar
function themename_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Before Footer Sidebar', 'theme_name' ),
        'id'            => 'before_footer',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="widget-title heading-strike">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'themename_widgets_init' );

add_filter( 'wpiw_list_class', 'my_instagram_class' );
function my_instagram_class( $classes ) {
	$classes = "c-instagram-flex";
	return $classes;
}
