<?php

/**
 * Change 'Related products' text strings
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
// add_filter( 'gettext', 'siteorigin_unwind_child_change_text_string', 20, 3 );
function siteorigin_unwind_child_change_text_string( $translated_text, $text, $domain ) {
    switch ( $text ) {
        case 'Related products' :
            $text = __( 'Complements your look', 'siteorigin-unwind-growpress-child' );
            break;
    }
    return $text;
}


/**
 * Change 'Select Option' text strings
 */
add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
    global $product;
    $product_type = $product->product_type;
    switch ( $product_type ) {
        case 'variable':
            return __( 'Buy', 'woocommerce' );
            break;
    }
}
