<?php

/**
 * Remove editor support for Woocommerce Product post type.
 *
 * @see https://developer.wordpress.org/reference/functions/remove_post_type_support/   Docs
 */

add_action( 'init', 'sogpct_rem_editor_from_post_type' );

function sogpct_rem_editor_from_post_type() {
    remove_post_type_support( 'product', 'editor' );
}
