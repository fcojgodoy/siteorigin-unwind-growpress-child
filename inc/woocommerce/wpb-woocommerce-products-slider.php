<?php

/**
 * Remove action from WPDB Woocommerce Products Slider plugin
 */

remove_action( 'wp_enqueue_scripts', 'wpb_wps_adding_dynamic_styles' );
