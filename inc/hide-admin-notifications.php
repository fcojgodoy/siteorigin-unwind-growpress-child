<?php

// Hide core updates notifications in the dashboard
function siteorigin_unwind_child_hide_wp_update_nag() {
    remove_action( 'admin_notices', 'update_nag', 3 ); //update notice at the top of the screen
    remove_filter( 'update_footer', 'core_update_footer' ); //update notice in the footer
}
add_action('admin_menu','siteorigin_unwind_child_hide_wp_update_nag');

// Hide TGMPA notifications to non admin users
function siteorigin_unwind_child_hide_tgmpa_msg_non_admins(){
    if (!current_user_can( 'manage_options' )) { // non-admin users
           echo '<style>#setting-error-tgmpa { display: none; }</style>';
       }
   }
add_action( 'admin_head', 'siteorigin_unwind_child_hide_tgmpa_msg_non_admins');
